'''

S04 Activity:

1. Create an abstract class called Animal that has the following abstract methods:
Abstract Methods: eat(food), make_sound()

2. Create two classes that implements the Animal class called Cat and Dog with each of the following properties and methods:

Properties: name, breed, age
Methods: getters and setters, implementation of abstract methods, call()

'''

# [ SOLUTION ]

from abc import ABC, abstractmethod

class Animal(ABC):
    def __init__(self, name, breed, age):
        self.name = name
        self.breed = breed
        self.age = age

    @abstractmethod
    def eat(self, food):
        pass

    @abstractmethod
    def make_sound(self):
        pass

class Cat(Animal):
    def eat(self, food):
        print(f"Serve me {food}")

    def make_sound(self):
        print(f"Miaow! Nyaw! Nyaaaa!")

    def call(self):
        print(f"{self.name}, come on!")

class Dog(Animal):
    def eat(self, food):
        print(f"Eaten {food}")

    def make_sound(self):
        print(f"Bark! Woof! Arf!")

    def call(self):
        print(f"Here {self.name}!")


# [ TEST CASES ]

dog = Dog("Isis", "Bulldog", 3)
cat = Cat("Puss", "Persian", 9)

dog.eat("Steak")
dog.make_sound()
dog.call()

cat.eat("Tuna")
cat.make_sound()
cat.call()
